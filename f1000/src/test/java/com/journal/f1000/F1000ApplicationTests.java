package com.journal.f1000;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.journal.f1000.model.Journal;
import com.journal.f1000.service.RankService;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class F1000ApplicationTests {

	@Test
	public void testScenario1() {
		
		ArrayList<Journal> input = new ArrayList<Journal>();
		
		Journal journalA = new Journal("Journal A", 5.6, false);
		Journal journalB = new Journal("Journal B", 2.4, false);
		Journal journalC = new Journal("Journal C", 3.1, false);
		
		input.add(journalA);
		input.add(journalB);
		input.add(journalC);
		
		ArrayList<Journal> output = RankService.rankJournals(input);
		
		
		Journal expA = new Journal("Journal A", 5.6, 1 ,false);
		Journal expB = new Journal("Journal B", 2.4, 3 ,false); 
		Journal expC = new Journal("Journal C", 3.1, 2 ,false); 
		
		ArrayList<Journal> expected = new ArrayList<Journal>();
		
		expected.add(expA);
		expected.add(expC);
		expected.add(expB);
		
		
		for(int i=0;i<output.size();i++) {
			
			Assert.assertEquals(expected.get(i).getName(), output.get(i).getName());
			Assert.assertEquals(expected.get(i).getScore(), output.get(i).getScore());
			Assert.assertEquals(expected.get(i).getRank(), output.get(i).getRank());
			Assert.assertEquals(expected.get(i).isReview(), output.get(i).isReview());

		}
		
	}
	
	@Test
	public void testScenario2() {
		
		ArrayList<Journal> input = new ArrayList<Journal>();
		
		Journal journalA = new Journal("Journal A", 2.2, false);
		Journal journalB = new Journal("Journal B", 6.2, false);
		Journal journalC = new Journal("Journal C", 6.2, false);
		
		input.add(journalA);
		input.add(journalB);
		input.add(journalC);
		
		ArrayList<Journal> output = RankService.rankJournals(input);

		
		Journal expA = new Journal("Journal A", 2.2, 3 ,false);
		Journal expB = new Journal("Journal B", 6.2, 1 ,false); 
		Journal expC = new Journal("Journal C", 6.2, 1 ,false); 
		
		ArrayList<Journal> expected = new ArrayList<Journal>();
		
		expected.add(expB);
		expected.add(expC);
		expected.add(expA);
		
		for(int i=0;i<output.size();i++) {
			
			Assert.assertEquals(expected.get(i).getName(), output.get(i).getName());
			Assert.assertEquals(expected.get(i).getScore(), output.get(i).getScore());
			Assert.assertEquals(expected.get(i).getRank(), output.get(i).getRank());
			Assert.assertEquals(expected.get(i).isReview(), output.get(i).isReview());


		}

	}
	
	@Test
	public void testScenario3() {
		
		ArrayList<Journal> input = new ArrayList<Journal>();
		
		Journal journalA = new Journal("Journal A", 5.6, true);
		Journal journalB = new Journal("Journal B", 2.4, false);
		Journal journalC = new Journal("Journal C", 3.1, false);
		
		input.add(journalA);
		input.add(journalB);
		input.add(journalC);
		
		ArrayList<Journal> output = RankService.rankJournals(input);

		Journal expB = new Journal("Journal B", 2.4, 2 ,false); 
		Journal expC = new Journal("Journal C", 3.1, 1 ,false); 
		
		ArrayList<Journal> expected = new ArrayList<Journal>();
		
		expected.add(expC);
		expected.add(expB);
		
		for(int i=0;i<output.size();i++) {
			
			Assert.assertEquals(expected.get(i).getName(), output.get(i).getName());
			Assert.assertEquals(expected.get(i).getScore(), output.get(i).getScore());
			Assert.assertEquals(expected.get(i).getRank(), output.get(i).getRank());
			Assert.assertEquals(expected.get(i).isReview(), output.get(i).isReview());


		}

	}
	

}

