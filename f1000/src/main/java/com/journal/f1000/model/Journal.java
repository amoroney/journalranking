package com.journal.f1000.model;

public class Journal {
	
	 String name;
	 double score;
	 int rank;
	 boolean review;
	
	public Journal() {
		super();
	}

	public Journal(String name, double score, boolean review) {
		super();
		this.name = name;
		this.score = score;
		this.review = review;
	}
	
	public Journal(String name, double score, int rank, boolean review) {
		super();
		this.name = name;
		this.score = score;
		this.rank = rank;
		this.review = review;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public boolean isReview() {
		return review;
	}
	public void setReview(boolean review) {
		this.review = review;
	}
	

}
