package com.journal.f1000;

import java.util.ArrayList;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.journal.f1000.model.Journal;
import com.journal.f1000.service.RankService;

@SpringBootApplication
public class F1000Application implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(F1000Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		Journal journalA = new Journal("Journal A", 2.2, true);
		Journal journalB = new Journal("Journal B", 2.4, false);
		Journal journalC = new Journal("Journal C", 3.1, false);


		ArrayList<Journal> list  = new ArrayList<Journal>();
		
		list.add(journalA);
		list.add(journalB);
		list.add(journalC);
		
		ArrayList<Journal> result = RankService.rankJournals(list);
		
		for(Journal j : result) {
			System.out.println("Rank = "+j.getRank()+ " Name = "+j.getName()+" Score = "+j.getScore());
		}
		
	}

}

