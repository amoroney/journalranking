package com.journal.f1000.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.journal.f1000.model.Journal;

public class RankService {
	
	public static ArrayList<Journal> rankJournals(ArrayList<Journal> list){
		
		ArrayList<Journal> output = new ArrayList<Journal>();
		
		for(Journal j :list) {
			if(!j.isReview()) {
				output.add(j);
			}
		}

		if(output.isEmpty()) {
			return output;
		}
		
		Collections.sort(output, new Comparator<Journal>()
		{
			public int compare(Journal journalA, Journal journalB)
				{
					return Double.valueOf(journalB.getScore()).compareTo(journalA.getScore());
				}
		});
		
		int rank = 1;
		ArrayList<Journal> temp = new ArrayList<Journal>();
		double score = output.get(0).getScore();
		ArrayList<ArrayList<Journal>> x = new ArrayList<ArrayList<Journal>>();
		
		for(int i=0;i<output.size();i++) {
			
			Journal j = output.get(i);
			
			if(j.getScore()==score) {
				j.setRank(rank);
				temp.add(j);
				Collections.sort(temp, new Comparator<Journal>()
				{
					public int compare(Journal journalA, Journal journalB)
						{
							return String.valueOf(journalA.getName()).compareTo(journalB.getName());
						}
				});
			}else {				
				x.add(temp);
				rank = i+1;
				temp = new ArrayList<Journal>();
				j.setRank(rank);
				temp.add(j);
			}
		}
		x.add(temp);
		ArrayList<Journal> result = new ArrayList<Journal>();
		
	
		for(ArrayList<Journal> jList : x) {
			result.addAll(jList);
		}
		
		return output;
	}

}
